package bianca;


import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Parser {

	public static Polinom transformare(String p) {
		Polinom result = new Polinom();
		String regex = "([+-]\\d+)[a-zA-Z]\\^(\\d+)";
		String regex2 = "([+-])[a-zA-z]\\^(\\d+)";
		String regex3="([+-]\\d+)[a-zA-Z]";
		String regex4="([+-])[a-zA-Z]";
		String regex5="[+-]\\d+";
		Pattern pattern = Pattern.compile(regex);

		Matcher matcher = pattern.matcher(p);

		while (matcher.find()) {
				int coef = Integer.parseInt(matcher.group(1));
				int putere = Integer.parseInt(matcher.group(2));
				Monom monom=new Monom(coef,putere);
				result.getPolinom().add(monom);

		}
		
		pattern = Pattern.compile(regex2);

		matcher = pattern.matcher(p);
        
		while (matcher.find()) {
			int coef;
			if(matcher.group(1).equals("-")){
				coef = -1;
			}
			else{
				coef=1;
			}
				
				int putere = Integer.parseInt(matcher.group(2));
				Monom monom=new Monom(coef,putere);
				result.getPolinom().add(monom);

		}

		return result;
	}

}
