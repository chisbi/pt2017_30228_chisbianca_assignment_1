package bianca;


public class Service {

	public static String adunare(String pol1, String pol2) {

		
		Polinom p1 = Parser.transformare(pol1);
		Polinom p2 = Parser.transformare(pol2);
		Polinom result = Operatie.adunare(p1, p2);
		return result.afisare();
	}

	public static String scadere(String pol1, String pol2) {

		Polinom p1 = Parser.transformare(pol1);
		Polinom p2 = Parser.transformare(pol2);
		Polinom result = Operatie.scadere(p1, p2);

		return result.afisare();
	}

	public static String inmultire(String pol1, String pol2) {

		Polinom p1 = Parser.transformare(pol1);
		Polinom p2 = Parser.transformare(pol2);
		Polinom result = Operatie.inmultire(p1, p2);

		return result.afisare();
	}

	public static String derivare(String pol1) {

		Polinom p1 = Parser.transformare(pol1);

		Polinom result = Operatie.derivare(p1);

		return result.afisare();
	}

	public static String integrare(String pol1) {

		Polinom p1 = Parser.transformare(pol1);

		Polinom result = Operatie.integrare(p1);

		return result.afisareDouble();
	}

}

