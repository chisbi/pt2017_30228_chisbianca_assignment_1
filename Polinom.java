package bianca;

import java.util.List;
import java.util.ArrayList;
import java.util.Collections;

public class Polinom {

	private List<Monom> polinom = new ArrayList<Monom>();

	public List<Monom> getPolinom() {
		return polinom;
	}

	public String afisare() {

		String afis_pol = new String("");
		for (int i = 0; i < polinom.size(); i++) {
			if (polinom.get(i).getCoef() < 0) {

				afis_pol += polinom.get(i).getMonom();
			} else if(polinom.get(i).getCoef()>0) {

				afis_pol += (0 == i) ? polinom.get(i).getMonom() : "+" + polinom.get(i).getMonom();
			}
		}
		return afis_pol;

	}

	public String afisareDouble() {
		String afis_pol = new String("");
		for (int i = 0; i < polinom.size(); i++) {
			if (polinom.get(i).getCoefD() < 0) {

				afis_pol += polinom.get(i).getMonomDouble();
			} else if(polinom.get(i).getCoefD()>0){

				afis_pol += (0 == i) ? polinom.get(i).getMonomDouble() : "+" + polinom.get(i).getMonomDouble();
			}
		}
		return afis_pol;

	}

	public void minimizare() {

		for (int i = 0; i < polinom.size()-1; i++) {

			int putere = polinom.get(i).getPutere();
			for (int j = i + 1; j < polinom.size(); j++) {

				int temp_putere = polinom.get(j).getPutere();
				if (putere == temp_putere) {

					polinom.get(i).setCoef(polinom.get(j).getCoef() + polinom.get(i).getCoef());
					this.polinom.remove(j);
				}
			}
		}
		Collections.sort(polinom);

	}
	
	 public int getIndexPutere(int putere) {

	       for (int i=0; i<polinom.size(); i++) {

	           Monom mon = polinom.get(i);
	           if (putere == mon.getPutere())
	               return i;
	       }
	       return -1;
	    }

}
