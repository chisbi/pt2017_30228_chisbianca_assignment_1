package bianca;


public class Operatie {

	public static Polinom adunare(Polinom a, Polinom b) {
		 Polinom result = new Polinom(); //rezultatul pe care il returnam
		 a.minimizare(); //sortam si minimizam polinoamele
		 b.minimizare();
	       for (int i=0; i<a.getPolinom().size(); i++) {

	           //Polinoamele sunt sortate si minime

	           int putere1 = a.getPolinom().get(i).getPutere();
	           int coef1 = a.getPolinom().get(i).getCoef();

	           int i_pow = b.getIndexPutere(putere1); //cautam in b monoame cu aceeasi putere 
	           if (i_pow == -1 ) {

	               //Nu am gasit nimic in b, deci adaugam un nou monom
	        	   Monom monom=new Monom(coef1,putere1);
	               result.getPolinom().add(monom);
	           }
	           else {

	               //Am gasit ceva, atunci facem suma
	               int putere3 = b.getPolinom().get(i_pow).getPutere();
	               int coef3 = b.getPolinom().get(i_pow).getCoef();
	               Monom monom=new Monom(coef1+coef3,putere3); //adunam coeficientii si puterea ramane
	               result.getPolinom().add(monom);

	               //Eliminam ce avem deja
	               b.getPolinom().remove(i_pow);
	           }
	       }
	             //Adaugam ce a ramas in b
	        for (int j=0; j<b.getPolinom().size(); j++) {

	           int putere2 = b.getPolinom().get(j).getPutere();
	           int coef2 = b.getPolinom().get(j).getCoef();
	           Monom monom=new Monom(coef2,putere2);
	           result.getPolinom().add(monom);
	        }
	      result.minimizare(); //sortam si minimizam rezultatul
		return result;
	}

	public static Polinom scadere(Polinom a, Polinom b) {
		Polinom result = new Polinom();

		Polinom aux = new Polinom(); //facem un auxiliar in care o sa avem monoamele din b dar cu coeficientii cu semn schimbat
		for (Monom monom : b.getPolinom()) {
			int putere = monom.getPutere();
			int coef = monom.getCoef()*(-1); //schimbam semnul coeficientilor din b
			Monom monAux = new Monom(coef, putere);
			aux.getPolinom().add(monAux);
		}
		result = adunare(a, aux);

		return result;
	}

	public static Polinom inmultire(Polinom a, Polinom b) {
		a.minimizare();
		b.minimizare();
		Polinom result = new Polinom();
		
	       for (int i=0; i<a.getPolinom().size(); i++) {
	    	   
	           Monom m1 = a.getPolinom().get(i);
	           for (int j=0; j<b.getPolinom().size(); j++) {
	               Monom m2 = b.getPolinom().get(j);
	               int coef=m1.getCoef()*m2.getCoef();
	               int putere=m1.getPutere()+m2.getPutere();
	               
	               Monom m3 = new Monom(coef,putere);

	               result.getPolinom().add(m3);
	           }
	       }
	       result.minimizare();
	       

		return result;
	}

	public static Polinom derivare(Polinom a) {
		Polinom result = new Polinom();

		
		return result;
	}

	public static Polinom integrare(Polinom a) {

		Polinom result = new Polinom();

	
		return result;
	}

	
}

